# Obsidian Theme

* Theme remade for BlizzCMS-Plus.
To get this Theme, contact Tyrael#4918 on Discord.

[![Project Version](https://img.shields.io/badge/Version-1.0-green.svg?style=flat-square)](#)

Theme for BlizzCMS-Plus based on Obsidian Theme of FusionCMS.

https://wow-cms.com/en/ - https://gitlab.com/WoW-CMS

# Project

* https://wow-cms.com/en/
* https://gitlab.com/WoW-CMS

## Designed

**_Designed By Tyrael#4918_**

# Requirements

**_BlizzCMS Plus_**

## Copyright

Copyright © 2021 [WoW-CMS](https://wow-cms.com)

# Screenshots

![Screenshot](Index.png)
![Screenshot](Store.png)
![Screenshot](Panel.png)
![Screenshot](Forum.png)